﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace hw5
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            places();
        }


        //function that adds pins on the map
        void places()
        {
            var bp = new Pin()
            {
                Position = new Position(15.997535, 120.225914), //sets the coordinates of the place
                Label = "My Birthplace", //sets the name of the pin
                Address = "Where I lived most of my childhood", //sets the adress or the description
                Type = PinType.Generic

            };

            var jp = new Pin()
            {
                Position = new Position(35.6905, 139.7738), //sets the coordinates of the place
                Label = "Tokyo, Japan", //sets the name of the pin
                Address = "Land of the Rising Sun", //sets the adress or the description
                Type = PinType.Generic
            };

            var rm = new Pin()
            {
                Position = new Position(32.911164, -117.148295), //sets the coordinates of the place
                Label = "Menya Ultra", //sets the name of the pin
                Address = "Best Japanese Tonkotsu Ramen in San Diego", //sets the adress or the description
                Type = PinType.Generic
            };

            var tp = new Pin()
            {
                Position = new Position(32.933866, -117.260656), //sets the coordinates of the place
                Label = "Torrey Pines State Beach", //sets the name of the pin
                Address = "Good place for hiking and relaxing at the beach", //sets the adress or the description
                Type = PinType.Generic
            };


            map.Pins.Add(bp);
            map.Pins.Add(jp);
            map.Pins.Add(rm);
            map.Pins.Add(tp);
                
        }

        //when the "street" button is clicked, the map type changes to Street view
        void street(object sender, EventArgs e)
        {
            map.MapType = MapType.Street;
            b.IsVisible = false;
            b2.IsVisible = true;
            b1.IsVisible = true;
        }

        //when the "satellite" button is clicked, the map type changes to Satellite view
        void satellite(object sender, EventArgs e)
        {
            map.MapType = MapType.Satellite;
            b1.IsVisible = false;
            b.IsVisible = true;
            b2.IsVisible = true;
        }

        //when the "hybrid" button is clicked, the map type changes to Hybrid view
        void hybrid(object sender, EventArgs e)
        {
            map.MapType = MapType.Hybrid;
            b2.IsVisible = false;
            b1.IsVisible = true;
            b.IsVisible = true;
        }

        //when the "pins" button is clicked, the picker shows up with different items that correspond to places
        void Location(object sender, EventArgs e)
        {
            if (!picker.IsFocused)
            {
                picker.Focus();
            }

        }

        //when an item is selected, the user is directed to that place
        void picker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            int selectedIndex = picker.SelectedIndex;

            if (selectedIndex != -1)
            {
                if (selectedIndex == 0)
                {
                    var pos = new Position(15.997535, 120.225914);
                    map.MoveToRegion(MapSpan.FromCenterAndRadius(pos, Distance.FromMeters(20)));

                }
                else if (selectedIndex == 1)
                {
                    var pos = new Position(35.6905, 139.7738);
                    map.MoveToRegion(MapSpan.FromCenterAndRadius(pos, Distance.FromMiles(50)));

                }
                else if (selectedIndex == 2)
                {
                    var pos = new Position(32.911164, -117.148295);
                    map.MoveToRegion(MapSpan.FromCenterAndRadius(pos, Distance.FromMeters(50)));
                }
                else if (selectedIndex == 3)
                {
                    var pos = new Position(32.933866, -117.260656);
                    map.MoveToRegion(MapSpan.FromCenterAndRadius(pos, Distance.FromMeters(50)));
                }
            }

        }
    }
}
